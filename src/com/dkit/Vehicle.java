package com.dkit;

import java.util.ArrayList;

/**
 * Created by Oshi on 01-Mar-18.
 */
public class Vehicle {
    int current_x;
    int current_y;
    int number_of_rides;
    boolean riding;
    boolean busy;
    Ride ride;
    int dis_to_depart;
    String ride_id;
    public Vehicle() {
        this.current_x = 0;
        this.current_y = 0;
        this.number_of_rides = 0;
        this.riding=false;
        this.busy=false;
        this.ride=  null;
        this.dis_to_depart=0;
        this.ride_id="";

    }

    public void setCurrent_x(int current_x) {
        this.current_x = current_x;
    }

    public int getCurrent_y() {
        return current_y;
    }

    public void setCurrent_y(int current_y) {
        this.current_y = current_y;
    }

    public int getNumber_of_rides() {
        return number_of_rides;
    }

    public void setNumber_of_rides(int number_of_rides) {
        this.number_of_rides = number_of_rides;
    }

    public boolean isRiding() {
        return riding;
    }

    public void setRiding(boolean riding) {
        this.riding = riding;
    }

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
    }

    public Ride getRide() {
        return ride;
    }

    public void setRide(Ride ride) {
        this.ride = ride;
        setDis_to_depart();
    }

    public int getDis_to_depart() {
        return dis_to_depart;
    }

    public void setDis_to_depart() {
        this.dis_to_depart =(Math.abs(this.current_x-this.ride.getStartIntersectionRow())+Math.abs(this.current_y-this.ride.getStartIntersectionCol()));
    }

    public String getRide_id() {
        return ride_id;
    }

    public void setRide_id(String ride_id) {
        this.ride_id = ride_id;
    }
}
