package com.dkit;

import java.io.Externalizable;
import java.io.File;
import java.io.FileOutputStream;
import java.util.*;

/**
 * 3 4 2 3 2 10
 * 0 0 1 3 2 9
 * 1 2 1 0 0 9
 * 2 0 2 2 0 9
 * 3 rows, 4 columns, 2 vehicles, 3 Rides, 2 bonus and 10 steps
 * ride from [0, 0] to [1, 3], earliest start 2, latest finish 9
 * ride from [1, 2] to [1, 0], earliest start 0, latest finish 9
 * ride from [2, 0] to [2, 2], earliest start 0, latest finish 9
 */


public class Main {

    static inputManager im;
    static ArrayList<Vehicle> vehicles;
    public static void main(String[] args) {

        String inFile = "b_should_be_easy.in";
        ArrayList<int[]> readLines = new ArrayList<>();
        try {
//            Scanner in = new Scanner(new File("b_should_be_easy.in"));
            Scanner in = new Scanner(new File(inFile));
            while (in.hasNext()) {
                String line = in.nextLine();
                readLines.add(Arrays.stream(line.split(" ")).mapToInt(Integer::parseInt).toArray());
//                System.out.println(line.length());
            }
//            System.out.println(readLines.size());
        } catch (Exception E) {
            System.err.println(E);
        }
        im = new inputManager(readLines);

//        System.out.println(im.getRides().size());

        List<Ride> rides = im.getRides();
        ArrayList<Ride> haventAssign= new ArrayList<Ride>();
        for(Ride r: rides){
            haventAssign.add(r);
        }
        intialiseVehicleList();
//        int count = 0;
//        for (Ride r :rides) {
//            if(!r.isAssigned()){
//            vehicles.get(count % inputManager.problemVariables.vehicles).addAssignedRide(r);
//            count++;
//            r.setAssigned(true);
//            }
//        }
        for(int i = 0; i < inputManager.problemVariables.timeSteps; i++){
            for(int j=0; j < vehicles.size(); j++){
                if(!vehicles.get(j).busy){
                    if(haventAssign.size() >0) {
                        int index = 0;
                        int min = vehicles.get(j).dis_to_depart + haventAssign.get(0).getDistance();
                        for (int a = 0; a < haventAssign.size(); a++) {
                            vehicles.get(j).setRide(haventAssign.get(a));
                            int total = vehicles.get(j).dis_to_depart + haventAssign.get(a).getDistance();
                            if (haventAssign.get(a).getEarliestStart() <= total + i || haventAssign.get(a).getLatestFinish() - i <= total) {
                                if (total < min) {
                                    min = total;
                                    index = a;
                                }
                            }
                        }
                        vehicles.get(j).ride = haventAssign.get(index);
                        haventAssign.remove(index);
                        rides.get(index).setAssigned(true);
                        vehicles.get(j).busy=true;
                        vehicles.get(j).number_of_rides= vehicles.get(j).number_of_rides+1;
                        vehicles.get(j).setRide_id(vehicles.get(j).getRide_id()+ vehicles.get(j).getRide().getRideID() + " ");
                    }
                }
                else if(!vehicles.get(j).riding && vehicles.get(j).busy) {
                    if (vehicles.get(j).ride.getStartIntersectionRow() == vehicles.get(j).current_x && vehicles.get(j).ride.getStartIntersectionCol() == vehicles.get(j).current_y) {
                        vehicles.get(j).riding = true;
                    } else {
                        if (vehicles.get(j).ride.getStartIntersectionRow() != vehicles.get(j).current_x) {
                            if (vehicles.get(j).ride.getStartIntersectionRow() > vehicles.get(j).current_x) {
                                vehicles.get(j).current_x = vehicles.get(j).current_x + 1;
                            } else {
                                vehicles.get(j).current_x = vehicles.get(j).current_x - 1;
                            }
                        } else if (vehicles.get(j).ride.getStartIntersectionCol() != vehicles.get(j).current_y) {
                            if (vehicles.get(j).ride.getStartIntersectionCol() > vehicles.get(j).current_y) {
                                vehicles.get(j).current_y = vehicles.get(j).current_y + 1;
                            } else {
                                vehicles.get(j).current_y = vehicles.get(j).current_y - 1;
                            }
                        }
                    }
                }
                if(vehicles.get(j).riding ) {
                    if (i >=vehicles.get(j).ride.getEarliestStart() ) {
                        if (vehicles.get(j).current_x != vehicles.get(j).ride.getFinishIntersectionRow()) {
                            if (vehicles.get(j).ride.getFinishIntersectionRow() > vehicles.get(j).current_x) {
                                vehicles.get(j).current_x = vehicles.get(j).current_x + 1;
                            } else {
                                vehicles.get(j).current_x = vehicles.get(j).current_x - 1;
                            }
                        } else if (vehicles.get(j).current_y != vehicles.get(j).ride.getFinishIntersectionCol()) {
                            if (vehicles.get(j).ride.getFinishIntersectionCol() > vehicles.get(j).current_y) {
                                vehicles.get(j).current_y = vehicles.get(j).current_y + 1;
                            } else {
                                vehicles.get(j).current_y = vehicles.get(j).current_y - 1;
                            }
                        }
                    }
                    if(vehicles.get(j).ride.getFinishIntersectionRow() == vehicles.get(j).current_x &&vehicles.get(j).ride.getFinishIntersectionCol() == vehicles.get(j).current_y){
                        vehicles.get(j).riding=false;
                        vehicles.get(j).busy=false;
                        vehicles.get(j).ride=null;
                        vehicles.get(j).dis_to_depart=0;
                        if(haventAssign.size() >0) {
                            int index = 0;
                            int min = vehicles.get(j).dis_to_depart + haventAssign.get(0).getDistance();
                            for (int a = 0; a < haventAssign.size(); a++) {
                                vehicles.get(j).setRide(haventAssign.get(a));
                                int total = vehicles.get(j).dis_to_depart + haventAssign.get(a).getDistance();
                                if (haventAssign.get(a).getEarliestStart() <= total + i || haventAssign.get(a).getLatestFinish() - i <= total) {
                                    if (total < min) {
                                        min = total;
                                        index = a;
                                    }
                                }
                            }
                            vehicles.get(j).ride = haventAssign.get(index);
                            haventAssign.remove(index);
                            rides.get(index).setAssigned(true);
                            vehicles.get(j).busy=true;
                            vehicles.get(j).number_of_rides= vehicles.get(j).number_of_rides+1;
                            vehicles.get(j).setRide_id(vehicles.get(j).getRide_id()+ vehicles.get(j).getRide().getRideID() + " ");
                        }
                    }



                }

            }
        }
        try{
        File outFile = new File(inFile+".out");
        FileOutputStream outputStream = new FileOutputStream(outFile);
            for(int i =1; i<=vehicles.size(); i++){
                System.out.println(i + " " + vehicles.get(i-1).ride_id);

            }
        }
        catch (Exception e){
            System.err.println(e);
        }
    }

    static void intialiseVehicleList(){
        vehicles = new ArrayList<>();
        for (int i = 0 ; i < inputManager.problemVariables.vehicles;i++){
            vehicles.add(new Vehicle());
        }
    }
}
